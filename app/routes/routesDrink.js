const express = require("express");

const {
    getAllDrinks,
    getDrinks,
    postDrinks,
    putDrinks,
    deleteDrinks
} =  require ("../middleware/middlewareDrink")

const router = express.Router()

router.use((req, res, next) => {
    console.log("Request URL Drinks: ", req.url);
    next();
})
router.get("/all" ,getAllDrinks ,(req, res) => {
    res.json({
        message: "Get all Drinks"
    })
});

router.get("/get" ,getDrinks ,(req, res) => {
    res.json({
        message: "Get a Drinks"
    })
});
router.post("/post" ,postDrinks ,(req, res) => {
    res.json({
        message: "POST a Drinks"
    })
});
router.put("/put" ,putDrinks ,(req, res) => {
    res.json({
        message: "PUT a Drinks"
    })
});
router.delete("/delete" ,deleteDrinks ,(req, res) => {
    res.json({
        message: "DELETE a Drinks"
    })
});

module.exports = {router};

