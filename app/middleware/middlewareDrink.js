const getAllDrinks = (req, res, next) => {
    console.log("Get all drinks");
    next();
}
const getDrinks = (req, res, next) => {
    console.log("Get a drinks");
    next();
};
const postDrinks = (req, res, next) => {
    console.log("Post a drinks");
    next();
}
const putDrinks = (req, res, next) => {
    console.log("Put a drinks");
    next();
}
const deleteDrinks = (req, res, next) => {
    console.log("Delete a drinks");
    next();
}

module.exports = {
    getAllDrinks,
    getDrinks,
    postDrinks,
    putDrinks,
    deleteDrinks
}