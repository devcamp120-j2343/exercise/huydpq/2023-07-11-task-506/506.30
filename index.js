const express =  require ("express");
const { router } = require("./app/routes/routesDrink");

const app = express();

const port = 8000;

app.use (router);

// app.get("/", (req, res) => {
//     console.log(req.method)
//     res.json({
//         message: "Chào"
//     })
// })  


app.listen(port,() => {
    console.log(`Đang chạy trên cổng: ${port}`)
})